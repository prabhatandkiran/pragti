from email.policy import default
from statistics import mode
from unittest.util import _MAX_LENGTH
from urllib import request
from django.db import models
from ckeditor.fields import *

# Create your models here.
class Enquiry(models.Model):
    enquiry_id=models.AutoField(primary_key=True)
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=50)
    email=models.EmailField(max_length=255)
    phone=models.CharField(max_length=20)
    subject=models.CharField(max_length=255, null=True, default=True)
    message=models.TextField(null=True, default=True)
    
class NewsLetter(models.Model):
    email_id=models.EmailField(max_length=255)
    created_at=models.DateField(auto_now_add=True)

class Slider(models.Model):
    title=models.CharField(max_length=100, null=True)
    sleder_desc=models.TextField(null=True)
    slider_image=models.ImageField(upload_to="images/slider")
    created_at=models.DateField(auto_now_add=True)

class Pages(models.Model):
    title=models.CharField(max_length=100, null=True)
    page_desc=models.TextField(null=True)
    page_content=RichTextField(null=True, blank=True)    
    created_at=models.DateField(auto_now_add=True)

class Gallery(models.Model):
    title=models.CharField(max_length=100, null=True)
    gallery_image=models.ImageField(upload_to="images/gallery")
    image_desc=models.TextField(null=True, default=True)    
    created_at=models.DateField(auto_now_add=True)

class Client(models.Model):
    name=models.CharField(max_length=100, null=True)
    client_image=models.ImageField(upload_to="images/clients")

class Certificate(models.Model):
    name=models.CharField(max_length=100, null=True)
    certificate_image=models.ImageField(upload_to="images/certificate")


class Volunter(models.Model):
    name=models.CharField(max_length=100, null=True)
    volunter_mobile=models.CharField(max_length=20, null=True, default=True)
    volunter_email=models.CharField(max_length=255, null=True, default=True)
    volunter_address=models.CharField(max_length=255, null=True, default=True)
    volunter_facebooks=models.CharField(max_length=255, null=True, default=True)
    volunter_skupe=models.CharField(max_length=255, null=True, default=True)
    volunter_google=models.CharField(max_length=255, null=True, default=True)
    volunter_image=models.ImageField(upload_to="images/volunters")
   