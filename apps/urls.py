from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name="index"),
    path('about-us/', views.aboutus, name="aboutus"),
    path('members/', views.members, name="members"),
    path('donate/', views.donate, name="donate"),
    path('volunter', views.volunter, name="volunter"),
    path('gallery/', views.gallery, name="gallery"),
    path('blog/', views.blog, name="blog"),
    path('contact-us/', views.contact, name="contact"),
    path('save-enquiry', views.save_enquiry),
    path('success/', views.successpage),
    path('subscribenewlatter/', views.subscribenewlatter)    
]
