from django.contrib import admin
from apps.models import *

# Register your models here.
class EnquiryAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone','email', 'subject')

admin.site.register(Enquiry, EnquiryAdmin)

class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ('email_id', 'created_at')

admin.site.register(NewsLetter, NewsLetterAdmin)

class SliderAdmin(admin.ModelAdmin):
    list_display = ('title','sleder_desc', 'slider_image', 'created_at')

admin.site.register(Slider, SliderAdmin)

class PagesAdmin(admin.ModelAdmin):
    list_display = ('title','page_desc', 'page_content', 'created_at')

admin.site.register(Pages, PagesAdmin)

class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title','gallery_image', 'image_desc', 'created_at')

admin.site.register(Gallery, GalleryAdmin)

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name','client_image')

admin.site.register(Client, ClientAdmin)

class VolunterAdmin(admin.ModelAdmin):
    list_display = ('name','volunter_mobile','volunter_email','volunter_address','volunter_image')

admin.site.register(Volunter, VolunterAdmin)

class CertificateAdmin(admin.ModelAdmin):
    list_display = ('name','certificate_image')

admin.site.register(Certificate, CertificateAdmin)