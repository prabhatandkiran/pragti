from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect
from django.contrib import messages
from apps.models import *


# Create your views here.

def index(request):
    sliders=Slider.objects.all()[:3]
    #about=Pages.objects.filter(title="About Us").values("page_content")
    about=Pages.objects.get(pk=1)
    gallery_images=Gallery.objects.all()[:12]
    partners_images=Client.objects.all()
    context={
        "sliders":sliders,
        "about":about,
        "gallery_images":gallery_images       
    }
   
    return render(request, "frontend/index.html", context)

def aboutus(request):
    return render(request, "frontend/about.html")

def members(request):
    return render(request, "frontend/members.html")

def donate(request):
    return render(request, "frontend/donate.html")

def volunter(request):
    volunters=Volunter.objects.all()   
    context={
        "volunters":volunters       
    }
    return render(request, "frontend/volunter.html", context)

def gallery(request):
    gallery_images=Gallery.objects.all()   
    context={
        "gallery_images":gallery_images       
    }
    return render(request, "frontend/gallary.html", context)

def contact(request):
    return render(request, "frontend/contact.html")

def blog(request):
    return render(request, "frontend/blog.html")

def save_enquiry(request):
    if request.method=="POST":        
        first_name=request.POST['first_name'] 
        last_name=request.POST['last_name']
        email=request.POST['email']
        phone=request.POST['phone']
        subject=request.POST['subject']
        message=request.POST['message']

        error_message={}
        error=False
        if first_name=="":
            error_message={
                "first_name":"First Name Filed is required"
            }
            error=True
        if last_name=="":
            error_message={
                "last_name":"Last Name Filed is required"
            }
            error=True
        if email=="":
            error_message={
                "email":"Email Filed is required"
            }
            error=True

        if error==True:
            messages.error(request, error_message)
            return HttpResponseRedirect("/contact-us")
        else:
            enquiry=Enquiry(first_name=first_name, last_name=last_name, email=email, phone=phone, subject=subject,message=message)
            enquiry.save();
            return HttpResponseRedirect("/success")
    else:
        return HttpResponseRedirect("/contact-us")
def successpage(request):
    return render(request, "frontend/success.html")

def subscribenewlatter(request):
    if request.method=="POST":
        email=request.POST['email']
        return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("/")

